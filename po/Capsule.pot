# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-16 15:58+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/nl.v0yd.Capsule.desktop.in:3 data/nl.v0yd.Capsule.metainfo.xml.in:8
#: src/window.ui:7
msgid "Capsule"
msgstr ""

#: data/nl.v0yd.Capsule.desktop.in:9
msgid "Medicine;Medication;Health;Doctor;Drugs;Pills;Swag;Treatment;Reminder"
msgstr ""

#: data/nl.v0yd.Capsule.metainfo.xml.in:9
msgid "Medication tracker"
msgstr ""

#: data/nl.v0yd.Capsule.metainfo.xml.in:11
msgid "Schedule and track your medication intake."
msgstr ""

#: data/nl.v0yd.Capsule.metainfo.xml.in:14
msgid ""
"Note: This app does not run in the background or send reminder notifications."
msgstr ""

#: data/nl.v0yd.Capsule.metainfo.xml.in:34
msgid "Today view"
msgstr ""

#: data/nl.v0yd.Capsule.metainfo.xml.in:38
msgid "List of medications"
msgstr ""

#: data/nl.v0yd.Capsule.metainfo.xml.in:42
msgid "Add medication dialog"
msgstr ""

#: src/main.js:70
msgid "translator-credits"
msgstr ""

#: src/main.js:79
msgid "Contributors"
msgstr ""

#: src/AddTherapyDialog.js:77
msgid "Edit Medication"
msgstr ""

#: src/AddTherapyDialog.js:78 src/AddTherapyDialog.js:123
msgid "Cancel"
msgstr ""

#: src/AddTherapyDialog.js:79
msgid "Update"
msgstr ""

#: src/AddTherapyDialog.js:122 src/window.ui:33
msgid "Add Medication"
msgstr ""

#: src/AddTherapyDialog.js:124
msgid "Add"
msgstr ""

#: src/AddTherapyDialog.js:137
msgid "One Pill"
msgstr ""

#: src/AddTherapyDialog.ui:36
msgid "Medication Name"
msgstr ""

#: src/AddTherapyDialog.ui:45
msgid "Daily"
msgstr ""

#: src/AddTherapyDialog.ui:52
msgid "Weekly"
msgstr ""

#: src/AddTherapyDialog.ui:73
msgid "Dosage"
msgstr ""

#: src/AddTherapyDialog.ui:89
msgid "Day of the Week"
msgstr ""

#. Repeat toggle for Monday
#: src/AddTherapyDialog.ui:99
msgctxt "day-of-the-week-monday"
msgid "M"
msgstr ""

#: src/AddTherapyDialog.ui:100
msgctxt "day-of-the-week"
msgid "Monday"
msgstr ""

#. Repeat toggle for Tuesday
#: src/AddTherapyDialog.ui:109
msgctxt "day-of-the-week-tuesday"
msgid "T"
msgstr ""

#: src/AddTherapyDialog.ui:110
msgctxt "day-of-the-week"
msgid "Tuesday"
msgstr ""

#. Repeat toggle for Wednesday
#: src/AddTherapyDialog.ui:118
msgctxt "day-of-the-week-wednesday"
msgid "W"
msgstr ""

#: src/AddTherapyDialog.ui:119
msgctxt "day-of-the-week"
msgid "Wednesday"
msgstr ""

#. Repeat toggle for Thursday
#: src/AddTherapyDialog.ui:127
msgctxt "day-of-the-week-thursday"
msgid "T"
msgstr ""

#: src/AddTherapyDialog.ui:128
msgctxt "day-of-the-week"
msgid "Thursday"
msgstr ""

#. Repeat toggle for Friday
#: src/AddTherapyDialog.ui:136
msgctxt "day-of-the-week-friday"
msgid "F"
msgstr ""

#: src/AddTherapyDialog.ui:137
msgctxt "day-of-the-week"
msgid "Friday"
msgstr ""

#. Repeat toggle for Saturday
#: src/AddTherapyDialog.ui:145
msgctxt "day-of-the-week-saturday"
msgid "S"
msgstr ""

#: src/AddTherapyDialog.ui:146
msgctxt "day-of-the-week"
msgid "Saturday"
msgstr ""

#. Repeat toggle for Sunday
#: src/AddTherapyDialog.ui:154
msgctxt "day-of-the-week-sunday"
msgid "S"
msgstr ""

#: src/AddTherapyDialog.ui:155
msgctxt "day-of-the-week"
msgid "Sunday"
msgstr ""

#: src/AddTherapyDialog.ui:169
msgid "Time"
msgstr ""

#: src/AddTherapyDialog.ui:173
msgid "Set Time"
msgstr ""

#: src/AddTherapyDialog.ui:234
msgid "Remind until"
msgstr ""

#: src/AddTherapyDialog.ui:255
msgid "Delete"
msgstr ""

#: src/Schedule.js:148
msgid "Every day"
msgstr ""

#: src/Schedule.js:150
#, javascript-format
msgid "Every <b>%s</b>"
msgstr ""

#: src/Schedule.js:153
#, javascript-format
msgid "%s at <b>%s</b> until <b>%s</b>"
msgstr ""

#: src/Schedule.js:155
#, javascript-format
msgid "%s at %s"
msgstr ""

#: src/Schedule.js:170
#, javascript-format
msgid "%s hours ago"
msgstr ""

#: src/Schedule.js:172
msgid "1 hour ago"
msgstr ""

#: src/Schedule.js:174
#, javascript-format
msgid "%s minutes ago"
msgstr ""

#: src/Schedule.js:176
msgid "1 minute ago"
msgstr ""

#: src/Schedule.js:178
msgid "right now"
msgstr ""

#: src/window.ui:29
msgid "Welcome to Capsule"
msgstr ""

#: src/window.ui:30
msgid "Manage your medication intake"
msgstr ""

#: src/window.ui:89
msgid "Today"
msgstr ""

#: src/window.ui:95
msgid "Nothing else today"
msgstr ""

#: src/window.ui:96
msgid "Medications will appear here on days where you have to take them"
msgstr ""

#: src/window.ui:129
msgid "Medications"
msgstr ""

#: src/window.js:186
#, javascript-format
msgid "Reminder to take %s of %s"
msgstr ""

#: src/window.js:188
#, javascript-format
msgid "Reminder to take %s"
msgstr ""
