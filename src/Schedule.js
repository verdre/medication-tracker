/* exported Schedule newFromExport */
const { Gio, GLib, GObject } = imports.gi;
const Format = imports.format;
String.prototype.format = Format.format;

var Schedule = new GObject.registerClass({
    Signals: {
        'expired': {},
        'finished': {},
    },
    Properties: {
        'taken-today': GObject.ParamSpec.boolean(
            'taken-today', 'Taken Today', 'Taken Today',
            GObject.ParamFlags.READWRITE,
            false),
        'happens-today': GObject.ParamSpec.boolean(
            'happens-today', 'Happens Today', 'Happens Today',
            GObject.ParamFlags.READABLE,
            false),
        'expired-today': GObject.ParamSpec.boolean(
            'expired-today', 'Expired Today', 'Expired Today',
            GObject.ParamFlags.READABLE,
            false),
        'human-friendly': GObject.ParamSpec.string(
            'human-friendly',
            'Human Friendly', 'Human Friendly',
            GObject.ParamFlags.READABLE,
            null),
        'human-friendly-delta': GObject.ParamSpec.string(
            'human-friendly-delta',
            'Human Friendly Delta', 'Human Friendly Delta',
            GObject.ParamFlags.READABLE,
            null),
    },
}, class Schedule extends GObject.Object {
    _init() {
        super._init({});

        this._weekday = null;

        this._hours = 0;
        this._minutes = 0;

        this._lastExpiryTime = null;
        this._nextExpiryTime = null;
        this._endDate = null;

        this._finished = false;

        this._happensToday = false;
        this._expiredToday = false;
        this._takenTodayTime = null;
        this._humanFriendlyDelta = "";
    }

    _updateExpiryTime(currentTime) {
        let expiryTime = currentTime.add_full(0, 0, 0,
            -currentTime.get_hour(), -currentTime.get_minute(),
            -currentTime.get_seconds());

        expiryTime = expiryTime.add_hours(this._hours);
        expiryTime = expiryTime.add_minutes(this._minutes);

        if (this._weekday) {
            expiryTime = expiryTime.add_days(-(expiryTime.get_day_of_week()));
            expiryTime = expiryTime.add_days(this._weekday);

            if (currentTime.compare(expiryTime) === 1)
                expiryTime = expiryTime.add_weeks(1);

            this._lastExpiryTime = expiryTime.add_weeks(-1);
        } else {
            if (currentTime.compare(expiryTime) === 1)
                expiryTime = expiryTime.add_days(1);

            this._lastExpiryTime = expiryTime.add_days(-1);
        }

        if (this._endDate) {
            if (expiryTime.compare(this._endDate) === 1) {
                this._finished = true;
                this.emit('finished');

                return;
            }
        }

        this._nextExpiryTime = expiryTime;
    }

    setWeekday(day) {
        this._weekday = day;

        const now = GLib.DateTime.new_now_local();
        this._updateExpiryTime(now);
        this._updateHappensToday(now);
        this._updateExpiredToday(now);
        this._updateHumanFriendlyDelta(now);
    }

    addTime(hours, minutes) {
        this._hours = hours;
        this._minutes = minutes;

        const now = GLib.DateTime.new_now_local();
        this._updateExpiryTime(now);
        this._updateHappensToday(now);
        this._updateExpiredToday(now);
        this._updateHumanFriendlyDelta(now);
    }

    setEndDate(date) {
        // We want to include the whole day of the end date
        this._endDate = date.add_days(1);

        const now = GLib.DateTime.new_now_local();
        this._updateExpiryTime(now);
        this._updateHappensToday(now);
        this._updateExpiredToday(now);
        this._updateHumanFriendlyDelta(now);
        if (this._finished)
            return false;

        return true;
    }

    getTime() {
        return [this._hours, this._minutes]
    }

    getNextExpiryTime() {
        return this._nextExpiryTime;
    }

    getLastExpiryTime() {
        return this._lastExpiryTime;
    }

    getWeekday() {
        return this._weekday;
    }

    getEndDate() {
        return this._endDate;
    }

    get human_friendly() {
        let dayString = _("Every day");
        if (this._weekday)
            dayString = _("Every <b>%s</b>").format(this._nextExpiryTime.format('%A'));

        if (this._endDate)
            return _("%s at <b>%s</b> until <b>%s</b>").format(dayString, this._nextExpiryTime.format('%R'), this._endDate.format('%F'));

        return _("%s at %s").format(dayString, this._nextExpiryTime.format('%R'));
    }

    _buildHumanFriendlyDelta(currentTime) {


        if (!this._expiredToday)
            return this._nextExpiryTime.format('%R');

        const delta = currentTime.difference(this._lastExpiryTime);
        const deltaSeconds = delta / 1000000;
        const deltaMinutes = deltaSeconds / 60;
        const deltaHours = deltaMinutes / 60;

        if (deltaHours > 2)
            return _("%s hours ago").format(Math.floor(deltaHours));
        else if (deltaHours > 1)
            return _("1 hour ago");
        else if (deltaMinutes > 2)
             return _("%s minutes ago").format(Math.floor(deltaMinutes));
        else if (deltaMinutes > 1)
             return _("1 minute ago");
        else
             return _("right now");
    }

    _updateHumanFriendlyDelta(currentTime) {
        const newDelta = this._buildHumanFriendlyDelta(currentTime);

        if (this._humanFriendlyDelta !== newDelta) {
            this._humanFriendlyDelta = newDelta;
            this.notify('human-friendly-delta');
        }
    }

    _updateHappensToday(currentTime) {
        const startOfToday = currentTime.add_full(0, 0, 0,
            -currentTime.get_hour(), -currentTime.get_minute(),
            -currentTime.get_seconds());
        const endOfToday = startOfToday.add_days(1);

        const happensToday =
            (endOfToday.compare(this._lastExpiryTime) === 1
             && startOfToday.compare(this._lastExpiryTime) === -1) ||
            (endOfToday.compare(this._nextExpiryTime) === 1
             && startOfToday.compare(this._nextExpiryTime) === -1);

        if (this._happensToday !== happensToday) {
            this._happensToday = happensToday;
            this.notify('happens-today');
        }
    }

    _updateExpiredToday(currentTime) {
        const startOfToday = currentTime.add_full(0, 0, 0,
            -currentTime.get_hour(), -currentTime.get_minute(),
            -currentTime.get_seconds());
        const endOfToday = startOfToday.add_days(1);

        const expiredToday =
            startOfToday.compare(this._lastExpiryTime) === -1
            && currentTime.compare(this._lastExpiryTime) === 1;

        if (this._expiredToday !== expiredToday) {
            this._expiredToday = expiredToday;
            this.notify('expired-today');
        }
    }

    _updateTakenToday(currentTime) {
        if (this._takenTodayTime === null)
            return;

        const startOfToday = currentTime.add_full(0, 0, 0,
            -currentTime.get_hour(), -currentTime.get_minute(),
            -currentTime.get_seconds());
        const endOfToday = startOfToday.add_days(1);

        const takenToday =
            startOfToday.compare(this._takenTodayTime) === -1
            && endOfToday.compare(this._takenTodayTime) === 1;

        if (!takenToday) {
            this._takenTodayTime = null;
            this.notify('taken-today');
        }
    }

    tick(currentTime) {
        if (!this._nextExpiryTime) {
            log("warning: tick without expiry time set");
            return;
        }

        this._updateHappensToday(currentTime);

        if (currentTime.compare(this._nextExpiryTime) === 1) {
            this.emit("expired");

            this._updateExpiryTime(currentTime);
        }

        this._updateExpiredToday(currentTime);
        this._updateTakenToday(currentTime);

        this._updateHumanFriendlyDelta(currentTime);
    }

    get human_friendly_delta() {
        return this._humanFriendlyDelta;
    }

    get happens_today() {
        return this._happensToday;
    }

    get expired_today() {
        return this._expiredToday;
    }

    set taken_today(taken) {
        if ((taken && this._takenTodayTime !== null) ||
            (!taken && this._takenTodayTime === null))
            return;

        this._takenTodayTime = taken
            ? GLib.DateTime.new_now_local()
            : null;

        this.notify('taken-today');
    }

    get taken_today() {
        return this._takenTodayTime !== null;
    }

    export() {
        return {
            time: [this._hours, this._minutes],
            endDate: this._endDate?.format_iso8601(),
            weekday: this._weekday,
            takenTodayTime: this._takenTodayTime?.format_iso8601(),
        };
    }
});

function newFromExport(obj) {
    const schedule = new Schedule();

    schedule.addTime(...obj.time);
    if (obj.endDate)
        schedule.setEndDate(GLib.DateTime.new_from_iso8601(obj.endDate, null));
    if (obj.weekday)
        schedule.setWeekday(obj.weekday);
    if (obj.takenTodayTime) {
        schedule._takenTodayTime = GLib.DateTime.new_from_iso8601(obj.takenTodayTime, null);
        schedule._updateTakenToday(GLib.DateTime.new_now_local());
    }

    return schedule;
}
