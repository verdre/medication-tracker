/* window.js
 *
 * Copyright 2022 Jonas Dreßler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Adw, GLib, GObject, Gio, Gtk } = imports.gi;
const Format = imports.format;
String.prototype.format = Format.format;

const Therapy = imports.Therapy;
const { AddTherapyDialog } = imports.AddTherapyDialog;

var DataDir = Gio.file_new_for_path(GLib.build_filenamev([GLib.get_user_data_dir(), pkg.name]));

var Sorter = GObject.registerClass({
}, class Sorter extends Gtk.Sorter {
    _init(params) {
        super._init(params);
    }

    vfunc_compare(therapyA, therapyB) {
        const timeA = therapyA.schedule.expired_today
            ? therapyA.schedule.getLastExpiryTime()
            : therapyA.schedule.getNextExpiryTime();
        const timeB = therapyB.schedule.expired_today
            ? therapyB.schedule.getLastExpiryTime()
            : therapyB.schedule.getNextExpiryTime();

        return timeA.compare(timeB);
    }
});

var CapsuleWindow = GObject.registerClass({
    GTypeName: 'CapsuleWindow',
    Template: 'resource:///nl/v0yd/Capsule/window.ui',
    InternalChildren: [
        'viewSwitcherTitle', 'mainStack', 'mainPage', 'emptyPage', 'overviewStack',
        'todayStack', 'emptyTodayPage', 'todayView', 'todayListBox',
        'therapiesView', 'therapiesListBox',
    ],
}, class CapsuleWindow extends Adw.ApplicationWindow {
    _init(application) {
        super._init({ application });

        this.connect('close-request', () => this._saveToStorageFile());

        this._timeNow = GLib.DateTime.new_now_local();

        this._therapies = new Gio.ListStore();

        this._todayFilter = new Gtk.CustomFilter();
        this._todayFilter.set_filter_func((therapy) => {
            return therapy.schedule.happens_today;
        });
        const todayFilterModel = new Gtk.FilterListModel({
            filter: this._todayFilter,
            model: this._therapies,
        });

        // GtkCustomSorter is broken in the JS bindings
        const sorter = new Sorter();
        const todaySortModel = new Gtk.SortListModel({
            model: todayFilterModel,
            sorter,
        });
        this._todayListBox.bind_model(todaySortModel, (therapy) => {
            const actionRow = new Adw.ActionRow({
                title: therapy.medication.name,
                subtitle: therapy.dose,
            });

            const box = new Gtk.Box();

            const label = new Gtk.Label();
            therapy.schedule.bind_property('human-friendly-delta',
                label, 'label', GObject.BindingFlags.SYNC_CREATE);

            const alarmIcon = new Gtk.Image({
                // icon_name: 'alarm-symbolic',
                icon_name: 'warning-symbolic',
                name: 'alarm-icon',
            });
            const revealer = new Gtk.Revealer({
                transition_type: Gtk.RevealerTransitionType.SLIDE_LEFT,
                child: alarmIcon,
            });
            let binding = therapy.schedule.bind_property('expired-today',
                revealer, 'reveal-child', GObject.BindingFlags.SYNC_CREATE);

            box.append(label);
            box.append(revealer);

            const checkButton = new Gtk.CheckButton({
                valign: Gtk.Align.CENTER,
            });
            checkButton.connect('toggled', () => {
                if (checkButton.active) {
                    actionRow.add_css_class('strikethrough');
                    actionRow.add_css_class('dim-label');
                    binding.unbind();
                    revealer.reveal_child = false;
                } else {
                    actionRow.remove_css_class('strikethrough');
                    actionRow.remove_css_class('dim-label');
                    binding = therapy.schedule.bind_property('expired-today',
                        revealer, 'reveal-child', GObject.BindingFlags.SYNC_CREATE);
                }
            });
            therapy.schedule.bind_property('taken-today',
                checkButton, 'active',
                GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL);

            actionRow.add_suffix(box);
            actionRow.add_prefix(checkButton);

            actionRow.activatable_widget = checkButton;

            return actionRow;
        });

        this._therapiesListBox.bind_model(this._therapies, (therapy) => {
            const actionRow = new Adw.ActionRow({
                title: therapy.medication.name,
                subtitle: therapy.schedule.human_friendly,
                activatable: true,
            });

            const icon = Gtk.Image.new_from_icon_name('document-edit-symbolic');

            actionRow.add_suffix(icon);
            actionRow.connect('activated', () => this._editTherapy(therapy));

            return actionRow;
        });

        this._therapies.connect('items-changed', () => {
            this._mainStack.visible_child = this._therapies.get_n_items() > 0
                ? this._mainPage : this._emptyPage;
        });
        this._mainStack.visible_child = this._emptyPage;

        todayFilterModel.connect('items-changed', () => {
            this._todayStack.visible_child = todayFilterModel.get_n_items() > 0
                ? this._todayView : this._emptyTodayPage;
        });
        this._todayStack.visible_child = this._emptyTodayPage;

        const actions = [
            { name: 'addTherapy', callback: this._addTherapy.bind(this), enabled: true },
        ];

        for (let { name, callback, enabled } of actions) {
            const action = new Gio.SimpleAction({ name, enabled });
            action.connect('activate', callback);
            this.add_action(action);
        }

        this._globalTimeoutId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 2, () => {
            return this._globalTimerTick(GLib.DateTime.new_now_local());
        });
        this._globalTimerTick(GLib.DateTime.new_now_local());
        GLib.Source.set_name_by_id(this._globalTimeoutId, '[gnome-shell] this._globalTimerTick');

        this._loadFromStorageFile();
    }

    _onScheduleExpired(therapy) {
        this._todayFilter.changed(Gtk.FilterChange.DIFFERENT);

        const notification = new Gio.Notification();
        notification.set_title(`Take ${therapy.medication.name}`);
        if (therapy.dose)
            notification.set_body(_("Reminder to take %s of %s").format(therapy.dose, therapy.medication.name));
        else
            notification.set_body(_("Reminder to take %s").format(therapy.medication.name));

        this.application.send_notification(this.application.application_id, notification);
    }

    _onScheduleFinished(therapy) {
        const [found, index] = this._therapies.find(therapy);
        this._therapies.remove(index);
    }

    _insertTherapy(therapy) {
        therapy.schedule.connect('expired', () =>
            this._onScheduleExpired(therapy));
        therapy.schedule.connect('finished', () =>
            this._onScheduleFinished(therapy));

        this._therapies.append(therapy);
    }

    _addTherapy() {
        new AddTherapyDialog(this.application, null, therapy => {
            this._insertTherapy(therapy);
        });
    }

    _editTherapy(therapy) {
        new AddTherapyDialog(this.application, therapy, updatedTherapy => {
            const [found, index] = this._therapies.find(therapy);
            this._therapies.remove(index);
            if (!updatedTherapy)
                return;

            this._insertTherapy(updatedTherapy);
        });
    }

    _globalTimerTick(timeNow) {
        const dayChanged = this._timeNow
            ? this._timeNow.get_day_of_week() !== timeNow.get_day_of_week()
            : true;
        this._timeNow = timeNow;

        for (const therapy of this._therapies)
            therapy.schedule.tick(timeNow);

        if (dayChanged)
            this._todayFilter.changed(Gtk.FilterChange.DIFFERENT);

        return GLib.SOURCE_CONTINUE;
    }

    _saveToStorageFile() {
        const storageObj = {};

        storageObj.therapies = [];
        for (const therapy of this._therapies)
            storageObj.therapies.push(therapy.export())

        try {
            DataDir.make_directory_with_parents(null);
        } catch (e) {
            if (!e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.EXISTS)) {
                log(`Failed to create data directory ${e}`);
                return;
            }
        }

        const file = DataDir.get_child('storage.json');
        const [success, tag] = file.replace_contents(
            JSON.stringify(storageObj), null, false,
            Gio.FileCreateFlags.REPLACE_DESTINATION, null);

        if (!success)
            log('Failed to save to storage file');
    }

    _loadFromStorageFile() {
        const file = DataDir.get_child('storage.json');
        if (!file.query_exists(null))
            return;

        const decoder = new TextDecoder('utf8');
        file.load_contents_async(null, (obj, res) => {
            const contents = obj.load_contents_finish(res)[1];
            let restoreData;
            try {
                restoreData = JSON.parse(decoder.decode(contents));
            } catch (error) {
                log('Error reading storage file');
                return;
            }

            for (const therapy of restoreData.therapies)
                this._insertTherapy(Therapy.newFromExport(therapy));
        });
    }
});
