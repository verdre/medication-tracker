/* exported Therapy newFromExport */
const { Gio, GLib, GObject } = imports.gi;

const Medication = imports.Medication;
const Schedule = imports.Schedule;

var Therapy = new GObject.registerClass({
    Signals: {
        'take-medication': {},
    },
    Properties: {
        'medication': GObject.ParamSpec.object(
            'medication',
            'Medication', 'The Medication object',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            Medication.Medication.$gtype),
        'dose': GObject.ParamSpec.string(
            'dose',
            'Dose', 'Dose of the medication',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            null),
        'schedule': GObject.ParamSpec.object(
            'schedule',
            'Schedule', 'The Schedule object',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            Schedule.Schedule.$gtype),
    },
}, class Therapy extends GObject.Object {
    get medication() {
        return this._medication;
    }

    set medication(medication) {
        if (this._medication === medication)
            return;

        this._medication = medication;

        this.notify('medication');
    }

    get dose() {
        return this._dose;
    }

    set dose(dose) {
        if (this._dose === dose)
            return;

        this._dose = dose;

        this.notify('dose');
    }

    get schedule() {
        return this._schedule;
    }

    set schedule(schedule) {
        if (this._schedule === schedule)
            return;

        this._schedule = schedule;

        this.notify('schedule');
    }

    toFancyString() {
        if (!this._dose)
            return this._schedule.toFancyString().replace(/^\w/, (c) => c.toUpperCase());

        return `<b>${this._dose}</b> ${this._schedule.toFancyString()}`;
    }

    export() {
        return {
            medication: this._medication.export(),
            schedule: this._schedule.export(),
            dose: this._dose,
        };
    }
});

function newFromExport(obj) {
    return new Therapy({
        medication: Medication.newFromExport(obj.medication),
        schedule: Schedule.newFromExport(obj.schedule),
        dose: obj.dose,
    });
}
