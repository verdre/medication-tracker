const { Adw, GLib, GObject, Gio, Gtk } = imports.gi;

const { Medication } = imports.Medication;
const { Schedule } = imports.Schedule;
const { Therapy } = imports.Therapy;

var AddTherapyDialog = GObject.registerClass({
    GTypeName: 'AddTherapyDialog',
    Template: 'resource:///nl/v0yd/Capsule/AddTherapyDialog.ui',
    InternalChildren: [
        'cancelButton', 'confirmButton', 'deleteButton',
        'nameInput', 'dailyToggleButton', 'weeklyToggleButton',
        'weeklySundayButton', 'weeklyMondayButton', 'weeklyTuesdayButton',
        'weeklyWednesdayButton', 'weeklyThursdayButton', 'weeklyFridayButton',
        'weeklySaturdayButton',
        'timeInputMenuButton', 'timeInputHours', 'timeInputMinutes',
        'endDateExpander', 'endDateCalendar',
        'dosageInput',
    ],
}, class AddTherapyDialog extends Gtk.Dialog {
    _init(application, existingTherapy, callback) {
        super._init({
            use_header_bar: true,
            application,
            transient_for: application.get_active_window(),
        });

        this._cancelButton.connect('clicked', () => this._cancel())
        this._confirmButton.connect('clicked', () => this._confirm())
        this._deleteButton.connect('clicked', () => this._delete())

        this._callback = callback;

        const onSpinButtonOutput = spinButton => {
            spinButton.text = this._addLeadingZero(spinButton.adjustment.value);
            return true;
        };

        this._timeInputHours.connect('output', onSpinButtonOutput);
        this._timeInputMinutes.connect('output', onSpinButtonOutput);

        this._timeInputHours.connect('value-changed', () => this._updateTimeInputLabel());
        this._timeInputMinutes.connect('value-changed', () => this._updateTimeInputLabel());

        this._nameInput.connect('notify::text', () =>
            this._nameInput.remove_css_class('error'));

        this._endDateExpander.connect('notify::expanded', () => this._updateExpanderState());
        this._endDateCalendar.connect('day-selected', () => this._updateExpanderState());

        this._fillDefaults(existingTherapy);

        this._nameInput.grab_focus();

        this.show();
    }

    _addLeadingZero(value) {
        return parseInt(value).toString().padStart(2, '0');
    }

    _updateTimeInputLabel() {
        this._timeInputMenuButton.label =
            this._timeInputHours.text + "\u2236" + this._timeInputMinutes.text;
    }

    _updateExpanderState() {
        this._endDateExpander.subtitle = this._endDateExpander.enable_expansion
            ? `Last day of the therapy is ${this._endDateCalendar.get_date().format('%F')}`
            : "Remind until medication is deleted";

        this._endDateExpander.remove_css_class('error');
    };

    _fillDefaults(existingTherapy) {
        if (existingTherapy) {
            this.title = _("Edit Medication");
            this._cancelButton.label = _("Cancel");
            this._confirmButton.label = _("Update");

            this._nameInput.text = existingTherapy.medication.name;

            const weekday = existingTherapy.schedule.getWeekday();
            if (weekday) {
                this._weeklyToggleButton.active = true;

                if (weekday === 7)
                    this._weeklySundayButton.active = true;
                else if (weekday === 1)
                    this._weeklyMondayButton.active = true;
                else if (weekday === 2)
                    this._weeklyTuesdayButton.active = true;
                else if (weekday === 3)
                    this._weeklyWednesdayButton.active = true;
                else if (weekday === 4)
                    this._weeklyThursdayButton.active = true;
                else if (weekday === 5)
                    this._weeklyFridayButton.active = true;
                else if (weekday === 6)
                    this._weeklySaturdayButton.active = true;
            }

            const [hours, minutes] = existingTherapy.schedule.getTime();
            this._timeInputHours.value = hours;
            this._timeInputMinutes.value = minutes;
            this._timeInputHours.text = this._addLeadingZero(hours);
            this._timeInputMinutes.text = this._addLeadingZero(minutes);
            this._updateTimeInputLabel();

            const endDate = existingTherapy.schedule.getEndDate();
            if (endDate) {
                this._endDateExpander.enable_expansion = true;
                this._endDateCalendar.select_day(endDate);
            } else {
                this._endDateExpander.enable_expansion = false;
            }
            this._updateExpanderState();

            this._dosageInput.text = existingTherapy.dose;

        } else {
            this.title = _("Add Medication");
            this._cancelButton.label = _("Cancel");
            this._confirmButton.label = _("Add");
            this._deleteButton.hide();

            const now = GLib.DateTime.new_now_local();
            this._timeInputHours.value = now.get_hour();
            this._timeInputMinutes.value = now.get_minute();
            this._timeInputHours.text = this._addLeadingZero(now.get_hour());
            this._timeInputMinutes.text = this._addLeadingZero(now.get_minute());
            this._updateTimeInputLabel();

            this._endDateExpander.enable_expansion = false;
            this._updateExpanderState();

            this._dosageInput.text = _("One Pill");
        }
    }

    _confirm() {
        const medicationName = this._nameInput.text;
        if (!medicationName) {
            this._nameInput.add_css_class('error');
            this._nameInput.grab_focus();
            return;
        }

        // ISO 8601, 1 is Monday, 7 is Sunday
        let day = null;

        if (this._weeklyToggleButton.active) {
            if (this._weeklySundayButton.active)
                day = 7;
            else if (this._weeklyMondayButton.active)
                day = 1;
            else if (this._weeklyTuesdayButton.active)
                day = 2;
            else if (this._weeklyWednesdayButton.active)
                day = 3;
            else if (this._weeklyThursdayButton.active)
                day = 4;
            else if (this._weeklyFridayButton.active)
                day = 5;
            else if (this._weeklySaturdayButton.active)
                day = 6;
        }

        let endDate = null;
        if (this._endDateExpander.enable_expansion)
            endDate = this._endDateCalendar.get_date();

        const hours = this._timeInputHours.value;
        const minutes = this._timeInputMinutes.value;
        const dosageString = this._dosageInput.text;

        const schedule = new Schedule();
        schedule.addTime(hours, minutes);
        if (day)
            schedule.setWeekday(day);
        if (endDate && !schedule.setEndDate(endDate)) {
            this._endDateExpander.expanded = true;
            this._endDateExpander.add_css_class('error');
            this._endDateCalendar.grab_focus();
            return;
        }

        const medication = new Medication({
            name: medicationName,
        });

        const therapy = new Therapy({
            dose: dosageString,
            medication,
            schedule,
        });

        if (this._callback)
            this._callback(therapy);

        this.close();
    }

    _delete() {
        if (this._callback)
            this._callback(null);

        this.close();
    }

    _cancel() {
        this.close();
    }
});
