/* main.js
 *
 * Copyright 2022 Jonas Dreßler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pkg.initGettext();
pkg.initFormat();
pkg.require({
  'Gio': '2.0',
  'Gtk': '4.0',
  'Adw': '1',
});

const { Gio, Gtk, Adw } = imports.gi;

const { CapsuleWindow } = imports.window;

function main(argv) {
    const application = new Adw.Application({
        application_id: 'nl.v0yd.Capsule',
        flags: Gio.ApplicationFlags.FLAGS_NONE,
    });

    const showAboutDialogAction = new Gio.SimpleAction({
        name: "about",
        parameter_type: null,
    });
    showAboutDialogAction.connect("activate", () => {
        ShowAboutWindow(application);
    });
    application.add_action(showAboutDialogAction);

    application.connect('activate', app => {
        let activeWindow = app.activeWindow;

        if (!activeWindow) {
            activeWindow = new CapsuleWindow(app);
        }

        activeWindow.present();
    });

    return application.run(argv);
}

function ShowAboutWindow(application) {
    const dialog = new Adw.AboutWindow({
        transient_for: application.get_active_window(),
        application_name: "Capsule",
        developer_name: "Jonas Dreßler",
        copyright: "© 2023 Jonas Dreßler",
        license_type: Gtk.License.GPL_3_0_ONLY,
        version: pkg.version,
        application_icon: pkg.name,
        issue_url: "https://gitlab.gnome.org/verdre/medication-tracker/-/issues",
        // TRANSLATORS: eg. 'Translator Name <your.email@domain.com>' or 'Translator Name https://website.example'
        translator_credits: _("translator-credits"),
        developers: [
          "Jonas Dreßler",
        ],
        designers: [
            "Tobias Bernard <tbernard@gnome.org>",
        ],
    });

    dialog.add_credit_section(_("Contributors"), [
        "Sonny Piers",
    ]);

    dialog.present();
  }
